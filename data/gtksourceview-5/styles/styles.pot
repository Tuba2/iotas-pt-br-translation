msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-12-08 12:36+1000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. (itstool) path: iotas-alpha-bold-dark-high-contrast.xml/style-scheme@_name
#. (itstool) path: iotas-alpha-bold-high-contrast.xml/style-scheme@_name
#: iotas-alpha-bold-dark-high-contrast.xml:3
#: iotas-alpha-bold-high-contrast.xml:3
msgid "Bold Markup (High Contrast)"
msgstr ""

#. (itstool) path: iotas-alpha-bold.xml/style-scheme@_name
#: iotas-alpha-bold.xml:3
msgid "Bold Markup"
msgstr ""

#. (itstool) path: iotas-alpha-dark-high-contrast.xml/style-scheme@_name
#. (itstool) path: iotas-alpha-high-contrast.xml/style-scheme@_name
#: iotas-alpha-dark-high-contrast.xml:3
#: iotas-alpha-high-contrast.xml:3
msgid "Muted Markup (High Contrast)"
msgstr ""

#. (itstool) path: iotas-alpha.xml/style-scheme@_name
#: iotas-alpha.xml:3
msgid "Muted Markup"
msgstr ""

#. (itstool) path: iotas-mono-dark-high-contrast.xml/style-scheme@_name
#. (itstool) path: iotas-mono-high-contrast.xml/style-scheme@_name
#: iotas-mono-dark-high-contrast.xml:3
#: iotas-mono-high-contrast.xml:3
msgid "Monochrome (High Contrast)"
msgstr ""

#. (itstool) path: iotas-mono.xml/style-scheme@_name
#: iotas-mono.xml:3
msgid "Monochrome"
msgstr ""

#. (itstool) path: iotas-unstyled-dark-high-contrast.xml/style-scheme@_name
#. (itstool) path: iotas-unstyled-high-contrast.xml/style-scheme@_name
#: iotas-unstyled-dark-high-contrast.xml:3
#: iotas-unstyled-high-contrast.xml:3
msgid "Disabled (High Contrast)"
msgstr ""

#. (itstool) path: iotas-unstyled.xml/style-scheme@_name
#: iotas-unstyled.xml:3
msgid "Disable"
msgstr ""

