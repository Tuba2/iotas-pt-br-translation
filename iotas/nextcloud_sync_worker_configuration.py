from abc import ABC, abstractmethod


class NextcloudSyncWorkerConfiguration(ABC):
    """Nextcloud sync worker configuration interface."""

    @abstractmethod
    def get_nextcloud_endpoint() -> str:
        """Get Nextcloud endpoint.

        :return: Endpoint
        :rtype: str
        """
        raise NotImplementedError()

    @abstractmethod
    def set_nextcloud_endpoint(value: str) -> None:
        """Set Nextcloud endpoint.

        :param str value: New value
        """
        raise NotImplementedError()

    @abstractmethod
    def get_nextcloud_username() -> str:
        """Get Nextcloud username.

        :return: Username
        :rtype: str
        """
        raise NotImplementedError()

    @abstractmethod
    def set_nextcloud_username(value: str) -> None:
        """Set Nextcloud username.

        :param str value: New value
        """
        raise NotImplementedError()

    @abstractmethod
    def get_nextcloud_prune_threshold() -> int:
        """Get Nextcloud prune threshold.

        :return: Threshold
        :rtype: int
        """
        raise NotImplementedError()

    @abstractmethod
    def set_nextcloud_prune_threshold(value: int) -> None:
        """Set Nextcloud prune threshold.

        :param int value: New value
        """
        raise NotImplementedError()
